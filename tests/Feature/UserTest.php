<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * A basic feature test example.
     * @group user
     * @return void
     */
    public function test_success_follow_user()
    {
        $this->actingAs($this->user);

        $another_user = factory(User::class)->create();

        $another_user->subscribers()->attach($this->user->id);

        $response = $this->get(route('follow', ['user' => $another_user]));
        $response->assertStatus(302);
    }
}
