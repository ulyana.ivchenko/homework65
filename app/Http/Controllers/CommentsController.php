<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    /**
     * @param CommentRequest $request
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(CommentRequest $request, Post $post)
    {
        $validated = $request->validated();

        Auth::user();

        $comment = new Comment();
        $comment->body = $request->input('body');
        $comment->post_id = $post->id;
        $comment->user_id = $request->user()->id;
        $comment->save();

        return response()->json([
            'comment' => view('comments.comment', compact('comment', 'post'))->render()
        ], 201);
    }


    /**
     * @param Post $post
     * @param Comment $comment
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Post $post, Comment $comment)
    {
        $this->authorize('update', $comment);
        return view('comments.edit', compact('comment', 'post'));
    }

    /**
     * @param CommentRequest $request
     * @param $post
     * @param Comment $comment
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CommentRequest $request, $post, Comment $comment)
    {
        $comment->update($request->all());
        return redirect(route('posts.show', compact('comment', 'post')))->with('message', 'Comment updated!');
    }

    /**
     * @param $post
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($post, $id)
    {
        $comment = Comment::findOrFail($id);
        $this->authorize('delete', $comment);
        $comment->delete();
        return response()->json([
            'status' => 'ok'
        ], 200);
    }
}
