$(document).ready(function () {
    $('.delete').on('click', function (event) {
        const postId = $(this).attr('data-post-id');
        const token = $('input[type=hidden]').val();
        const post = $('#delete-post-' + postId);
        $.ajax({
            url: `posts/${postId}`,
            method: 'delete',
            data: {_token: token}
        })
            .done(function(response) {
                console.log('response => ', response);
                $(post).remove();
            })
            .fail(function(response) {
                console.log('fail response => ', response);
            });
    })
});
