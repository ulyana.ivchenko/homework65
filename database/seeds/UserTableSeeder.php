<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = factory(\App\User::class)->create();
        $user2 = factory(\App\User::class)->create();
        $user3 = factory(\App\User::class)->create();

        factory(\App\User::class, 20)->create();

        $user1->subscribers()->attach([rand(4, 20), rand(4, 20), rand(4, 20)]);
        $user2->subscribers()->attach([rand(4, 20), rand(4, 20), rand(4, 20)]);
        $user3->subscribers()->attach([rand(4, 20), rand(4, 20), rand(4, 20)]);
    }
}
