@extends('layouts.app')
@section('content')
    <div class="container">
        @if (session('message'))
            <div class="alert alert-primary" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <h3>{{$post->user->name}}</h3>
                <img class="img-fluid"
                     src="{{asset('/storage/' . $post->image)}}" alt="Photo">
                <blockquote class="blockquote pt-3">
                    <p class="mb-2" style="text-align: justify; font-size: 15px;">
                        {{$post->description}}
                    </p>
                </blockquote>
            </div>
        </div>

        <div class="row mt-4">
            <div id="comments-block" class="col-7">
                @csrf
                @foreach($post->comments as $comment)
                    @if($comment->approved == 1)
                    <div class="media" id="delete-comment-{{$comment->id}}">
                        <img class="d-flex rounded-circle mt-0 mr-3" style="width: 50px; height: 50px"
                             src="{{asset('/images/default_photo.jpeg')}}" alt="Image">
                        <div class="media-body">
                            <div class="mb-3">
                                @can('delete', $comment)
                                    <input type="hidden" id="post-id" value="{{$post->id}}">
                                    <span data-comment-id="{{$comment->id}}" class="delete" aria-hidden="true"
                                          style="float: right; font-size: 20px;"
                                          value="{{$post->id}}">&times;</span>
                                @endcan
                            </div>
                            <p class="pr-4">{{$comment->body}}</p>
                            <div class="comment">
                                <span>Commented: {{$comment->user->name}}, </span>
                                <span>{{$comment->created_at->diffForHumans()}}</span>
                            </div>
                            @can('update', $comment)
                                <div>
                                    <a href="{{route('posts.comments.edit', ['post' => $post, 'comment' => $comment])}}"
                                       style="font-size: 12px">Edit</a>
                                </div>
                            @endcan
                            <hr>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
            @if (Auth::user()->id !== $post->user->id)
                <div class="col-md-4 ml-auto mt-3">
                    <div class="comment-form">
                        <form id="add-comment">
                            @csrf
                            <input type="hidden" id="post_id" value="{{$post->id}}">
                            <div class="form-group">
                                <label for="body">Comment</label>
                                <textarea name="body" class="form-control" id="body" rows="3" required></textarea>
                            </div>
                            <button id="add-comment-btn" type="submit" class="btn btn-primary btn-block">Add comment
                            </button>
                        </form>
                    </div>
                </div>
            @endif
        </div>
        <div class="row p-5">
            <div class="col-10 offset-5">
                {{$post->comments->links()}}
            </div>
        </div>
    </div>
@endsection
