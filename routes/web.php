<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsersController@index')->name('home')->middleware('auth');

Route::resource('posts', 'PostsController')->middleware('auth');
Route::resource('posts.comments', 'CommentsController')->middleware('auth');
Route::resource('users', 'UsersController')->only('index')->middleware('auth');
Route::get('follow/{user}', 'UsersController@follow')->name('follow')->middleware('auth');
Route::resource('posts.likes', 'LikesController')->only('store')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
