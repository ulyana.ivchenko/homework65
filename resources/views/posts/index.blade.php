@extends('layouts.app')
@section('content')
    <div class="container">
        @if (session('message'))
            <div class="alert alert-primary" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <div class="row d-flex justify-content-center">
            @csrf
            @foreach($posts as $post)
                @foreach($users as $user)
                    @if($post->user->id == $user->id)
                        <div class="col-md-4 mt-4" id="delete-post-{{$post->id}}">
                            <div class="card ml-0 mr-0">
                                <div class="card-header d-flex justify-content-between">
                                    {{$post->user->name}}
                                </div>
                                <div class="card-body pb-1">
                                    <img class="img-fluid"
                                         src="{{asset('/storage/' . $post->image)}}" alt="Photo">
                                    <blockquote class="blockquote mb-0 pt-1">
                                        <p class="text-truncate mb-0" style="font-size: 15px;">
                                            {{ $post->description }}
                                        </p>

                                        <div class="d-flex justify-content-between pb-0">
                                            @if($post->likes->count() > 0)
                                                <a>
                                                    <i class="fa fa-heart" style="color: indianred"
                                                      aria-hidden="true"></i>
                                                    <span style="font-size: 12px"> {{$post->likes->count()}} </span>
                                                </a>
                                            @endif
                                        <a href="{{route('posts.show', ['post' =>$post])}}"
                                           class="card-link mt-2">More</a>
                                        </div>
                                        @can('update', $post)
                                            <a href="{{route('posts.edit', ['post' => $post])}}"
                                               class="card-link">Edit post</a>
                                        @endcan
                                    </blockquote>
                                </div>
                                <div class="card-footer">
                                    <div class="d-flex justify-content-between">
                                        <form method="post" action="{{route('posts.likes.store', ['post' => $post])}}">
                                            @csrf
                                            <input type="hidden" id="post_id" value="{{$post->id}}">
                                            <button type="submit" class="btn btn-outline-light active p-0">Like
                                            </button>
                                        </form>
                                        @if ($post->comments->count() == 1)
                                            <span> {{$post->comments->count()}} Comment</span>
                                        @elseif($post->comments->count() > 1 || $post->comments->count() == 0)
                                            <span> {{$post->comments->count()}} Comments</span>
                                        @endif
                                        @can('delete', $post)
                                            <input type="hidden" id="post-id" value="{{$post->id}}">
                                            <span data-post-id="{{$post->id}}" class="delete"
                                                  aria-hidden="true">&times;</span>
                                        @endcan
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endforeach
        </div>
    </div>
@endsection
